import { Component, OnInit } from '@angular/core';
import { Country } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html',
  styles: [
  ]
})
export class PorCapitalComponent implements OnInit {

  termino:string ="Mexico";
  error:boolean = false;
  paises: Country[] = [];

  constructor(private paisService: PaisService) { }

  ngOnInit(): void {
  }

  buscar(termino: string) {
    this.error = false;
    // asignando el termino a la propiedad de la clases
    this.termino = termino;
    this.paisService.buscarCapital(termino)
    .subscribe(paises => {
      // console.log(paises);
      this.paises = paises;


    },(err) => {
      console.log('Error');
      console.info(err);
      this.error = true;
      this.paises = [];
    });

  }

  sugerencias(termino: string) {
    this.error = false;
    //Crear sugerencias
  }

}
