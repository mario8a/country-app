import { Component, OnInit } from '@angular/core';
import { PaisService } from '../../services/pais.service';
import { Country } from '../../interfaces/pais.interface';

@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styles: [
    `
      li {
        cursor: pointer;
      }
    `
  ]
})
export class PorPaisComponent implements OnInit {

  termino:string ="Mexico";
  error:boolean = false;
  paises: Country[] = [];
  paisesSugeridos: Country[] = [];
  mostrarSugerencias: Boolean = false;

  constructor(private paisService: PaisService) { }

  ngOnInit(): void {
  }

  buscar(termino: string) {
    this.mostrarSugerencias = false;
    this.error = false;
    // asignando el termino a la propiedad de la clases
    this.termino = termino;
    this.paisService.buscarPais(termino)
    .subscribe(paises => {
      // console.log(paises);
      this.paises = paises;


    },(err) => {
      console.log('Error');
      console.info(err);
      this.error = true;
      this.paises = [];
    });

  }

  sugerencias(termino: string) {
    this.error = false;
    //Crear sugerencias
    this.termino = termino;
    this.mostrarSugerencias = true;

    this.paisService.buscarPais(termino)
      .subscribe(
        paises => this.paisesSugeridos = paises.splice(0,5),
        (err) => this.paisesSugeridos = []
      )
  }

  buscarSugerido(termino: string) {
    this.buscar(termino);
    
  }

}
