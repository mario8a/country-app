import { Component, EventEmitter, Output, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';


@Component({
  selector: 'app-pais-input',
  templateUrl: './pais-input.component.html',
  styles: [
  ]
})
export class PaisInputComponent implements OnInit {
  
  // A los eventos se les pone al inicio ""on"", en realidad se puede poner cualquier nombre
  @Output() onEnter: EventEmitter<string> = new EventEmitter();
  // DebounceTime, se emitira cuando la persona deja de escribir
  @Output() onDebounce : EventEmitter<string> = new EventEmitter();
  @Input() placeholder: string = '';
  
  debouncer: Subject<string> = new Subject();
  
  termino: string = '';
  
  ngOnInit() {
    // debounceTime(timpor de espera para emitir el siguiente valor)
    // Hasta que dejes de escribir pasaran esos 300 ml y se emitira el evento
    this.debouncer
      .pipe(debounceTime(300))
      .subscribe(valor => {
        this.onDebounce.emit(valor);
        console.log('Debouncer',  valor)
      })
  }

  buscar() {
    this.onEnter.emit(this.termino);
  }

  teclaPresionada() {
    // Con el next hace que se lance la suscribcion que esta en el ngOnInit
    this.debouncer.next(this.termino);
  }

}
